"use strict"

$(document).on("click", ".menu a", function(e) {
    e.preventDefault();
    var id  = $(this).attr('href');
    var top = $(id).offset().top; 
    $('body, html').animate({scrollTop: top}, 800);  
});


$(document).on("click", ".scroll-to-up", function(e) {
    e.preventDefault();
    $('body, html').animate({scrollTop: 0}, 800);
}); 

$(document).scroll(function(e) {
 

    if ($(document).scrollTop() > 40) {
        $('.navbar').css('position', 'fixed')
        $('.navbar').css('top', '0px')
        $('.navbar').css('left', '0px')
        $('.menu').css('background', '#37414b')
    }else {
        $('.menu').css('background', 'none')
        $('.navbar').css('position', 'relative')
    }

    if ($(document).scrollTop() > 200) {
       $('.scroll-up').css("display", "block")
    }else {
        $('.scroll-up').css("display", "none")
    }
})


 
 

 